**Provides click & drag for checkboxes on the permissions page to administer permissions faster.** Lightweight Drupal 8 alternative to [Fast permissions administration (FPA) module](https://www.drupal.org/project/fpa). Implements the wonderful Drag Check JS library fron [@scarlac](https://github.com/scarlac/): https://github.com/scarlac/drag-check-js for the permissions matrix page.

Alternatives:
-------------

*   [Fast permissions administration (FPA) module](https://www.drupal.org/project/fpa)
*   Browser plugins which address the same problem (poor for current FF
versions)

Installation
------------

1.  Have [scarlac's drag-check-js library](https://github.com/scarlac/drag-check-js) in the /libraries folder.
    There are multiple ways to achive that depending on your setup:
     - asset-packagist.org would be one of them, see [Downloading third-party libraries using Composer](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#third-party-libraries)
    - or you could define a composer package in your composer.json like:
```
{
  "type": "package",
  "package": {
    "name": "scarlac/drag-check-js",
    "version": "2.0.2",
    "type": "drupal-library",
    "dist": {
      "url": "https://github.com/scarlac/drag-check-js/archive/v2.0.2.zip",
      "type": "zip"
    }
  }
}
```
- and require it with
```
    composer require scarlac/drag-check-js drupal/permissions_dragcheck
```
2.  Download and enable this module
3.  Open the permission administration _admin/people/permissions_ and use like
described at "How to use"
4.  Done! Happy using!

Dependencies
------------

*   [Libraries module](https://www.drupal.org/project/libraries)

How to use:
-----------

*   Simply click & drag on a checkbox to check multiple checkboxes at once.

Development proudly sponsored by German Drupal Friends & Companies:
-------------------------------------------------------------------

[webks: websolutions kept simple](http://www.webks.de) (http://www.webks.de) and [DROWL: Drupalbasierte L?sungen aus Ostwestfalen-Lippe](http://www.drowl.de) (http://www.drowl.de)
