/**
 * @file
 * Provides JavaScript for permission_dragcheck
 */

(function ($) {
  Drupal.behaviors.permissions_dragcheck = {
    attach: function (context) {
      $('table#permissions :checkbox:not([readonly],[disabled])', context).change(function (e) {
        $(this).closest('td').css('background-color', $(this).is(':checked') ? '#73B355' : '#FFFACD');
      }).dragCheck();
    }
  };
})(jQuery);
